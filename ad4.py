import re


def count_list(lines: str):
    ranges = re.split(",|-", lines)
    set1 = {i for i in range(int(ranges[0]), int(ranges[1]) + 1)}
    set2 = {i for i in range(int(ranges[2]), int(ranges[3]) + 1)}
    return int(set1.issubset(set2) or set2.issubset(set1))


def count_any(lines: str):
    ranges = re.split(",|-", lines)
    set1 = {i for i in range(int(ranges[0]), int(ranges[1]) + 1)}
    set2 = {i for i in range(int(ranges[2]), int(ranges[3]) + 1)}
    return int(not set1.isdisjoint(set2))


if __name__ == "__main__":
    file = open("as.txt", "r")
    counter = 0
    for line in file:
        counter = counter + count_any(line)
        print("{} {}".format(line, count_list(line)))
    print(counter)

