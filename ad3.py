import string


def get_priority():
    letters = list(string.ascii_letters)
    values = [k + 1 for k in range(53)]
    return dict(zip(letters, values))


def calculate_value(letters: list):
    res = 0
    priority = get_priority()
    print(letters)
    for letter in letters:
        res = res + priority[letter]
        print(letter + ": " + str(priority[letter]))
    return res


def find_repeated(word: str):
    first_half = word[0:len(word) // 2]
    second_half = word[len(word) // 2 if len(word) % 2 == 0
                       else ((len(word) // 2) + 1):]
    return list(set(first_half) & set(second_half))


if __name__ == "__main__":
    file = open("rucksack.txt", "r")
    result = 0
    for line in file:
        repeated = find_repeated(line)
        result = result + calculate_value(repeated)
        print(result)
    print(result)
