import operator
from heapq import nlargest


def find_calories():
    calories = {}
    current_elf = 1
    file = open("calories.txt", "r")
    for line in file:
        if line == "\n":
            current_elf = current_elf + 1
        else:
            if current_elf in calories:
                calories[current_elf] = calories[current_elf] + int(line)
            else:
                calories[current_elf] = int(line)
    return calories


def find_max(calories):
    return max(calories.items(), key=operator.itemgetter(1))


def find_top_3(calories):
    keys = nlargest(3, calories, key=calories.get)
    return [calories[k] for k in keys]


if __name__ == "__main__":
    all_elf = find_calories()
    top_three = find_top_3(all_elf)
    print(find_max(all_elf))
    print(sum(top_three))

