def calculate_result(opponent_input: str, your_input: str):
    # A X rock B Y paper C Z scissors
    res = {
        ("A", "X"): (3, 1),
        ("A", "Y"): (6, 2),
        ("A", "Z"): (0, 3),
        ("B", "X"): (0, 1),
        ("B", "Y"): (3, 2),
        ("B", "Z"): (6, 3),
        ("C", "X"): (6, 1),
        ("C", "Y"): (0, 2),
        ("C", "Z"): (3, 3),
    }
    return sum(res[(opponent_input, your_input)])


def calculate_strategy(opponent_input: str, your_input: str):
    res = {
        ("A", "X"): (0, 3), #C
        ("A", "Y"): (3, 1), #A
        ("A", "Z"): (6, 2), #B
        ("B", "X"): (0, 1), #A
        ("B", "Y"): (3, 2), #B
        ("B", "Z"): (6, 3), #C
        ("C", "X"): (0, 2), #B
        ("C", "Y"): (3, 3), #C
        ("C", "Z"): (6, 1), #A
    }
    return sum(res[(opponent_input, your_input)])


def decode_input():
    first_strategy = 0
    second_strategy = 0
    file = open("rps.txt", "r")
    for line in file:
        val = line.split()
        first_strategy = first_strategy + calculate_result(val[0], val[1])
        second_strategy = second_strategy + calculate_strategy(val[0], val[1])
    return first_strategy, second_strategy


if __name__ == "__main__":
    first, second = decode_input()
    print(first)
    print(second)
